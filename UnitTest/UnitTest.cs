using NUnit.Framework;
using Moq;
using TestMoqApi.Services;
using TestMoqApi.Models.Dto;
using TestMoqApi.Business.Implementation;
using TestMoqApi.Filter.CustomExeption;
using TestMoqApi.Model.Token;

namespace UnitTests
{
    [TestFixture]
    public class Tests
    {
        private BusinessGet<object> businessGetTestFlowers;
        private object resultMock;
        private GenericToken Token;
        
        [SetUp]
        public void Setup()
        {
            var mockGetServices = new Mock<IGetServices<object>>();
            // Cria Objeto para resposta mockado
            resultMock = new { mockResponse = "Response Mock" };
            mockGetServices.Setup(x => x.GetFlowers(It.IsAny<GetFlowersDto>())).Returns(resultMock);
            mockGetServices.Setup(x => x.GetIceCream(It.IsAny<GetIceCreamDto>())).Returns(resultMock);
            // Passe o objeto Mock criado .Object
            businessGetTestFlowers = new BusinessGet<object>(mockGetServices.Object);
        }

        [TestCase()]
        public void SholdReturnCorrectResponseGetFlowers()
        {
            GetFlowersDto parameters = new GetFlowersDto();
            parameters.Name = "Blue";
            parameters.RandomNumber = 3;
            var result = businessGetTestFlowers.GetFlowers(parameters);
            Assert.AreEqual(result, resultMock);
        }

        [Test]
        public void ShouldReturnCustomExeptionBadRequestGetFlowers()
        {
            GetFlowersDto parameters = new GetFlowersDto();
            var exception = Assert.Throws<CustomCodeException>(() => businessGetTestFlowers.GetFlowers(parameters));
            Assert.AreEqual(400, exception.StatusCode);
            Assert.AreEqual(CustomCodeExceptionMensages.NameCantBeNull, exception.Message);
        }
        [Test]
        public void SholdReturnCorrectResponseGetIceCream()
        {
            GetIceCreamDto parameters = new GetIceCreamDto();
            parameters.Name = "Blue";
            parameters.RandomNumber = 3;
            var result = businessGetTestFlowers.GetIceCream(parameters);
            Assert.AreEqual(result, resultMock);
        }

        [Test]
        public void ShouldReturnCustomExeptionBadRequestGetIceCream()
        {
            GetIceCreamDto parameters = new GetIceCreamDto();
            var exception = Assert.Throws<CustomCodeException>(() => businessGetTestFlowers.GetIceCream(parameters));
            Assert.AreEqual(400, exception.StatusCode);
            Assert.AreEqual(CustomCodeExceptionMensages.NameCantBeNull, exception.Message);
        }
    }
}