﻿using System.Collections.Generic;

namespace TestMoqApi.StatusCode
{
    public static class ReturnStatusCode
    {
        public static List<StatusCode> GetStatusCode()
        {
            CollectionStatusCode collection = new CollectionStatusCode();

            collection.AddList(new List<StatusCode>()
            {
                            new StatusCode("202", "Accepted"),
                            new StatusCode("300", "Ambiguous"),
                            new StatusCode("502", "BadGateway"),
                            new StatusCode("400", "BadRequest"),
                            new StatusCode("409", "Conflict"),
                            new StatusCode("100", "Continue"),
                            new StatusCode("201", "Created"),
                            new StatusCode("417", "ExpectationFailed"),
                            new StatusCode("403", "Forbidden"),
                            new StatusCode("302", "Found"),
                            new StatusCode("504", "GatewayTimeout"),
                            new StatusCode("410", "Gone"),
                            new StatusCode("505", "HttpVersionNotSupported"),
                            new StatusCode("500", "InternalServerError"),
                            new StatusCode("411", "LengthRequired"),
                            new StatusCode("405", "MethodNotAllowed"),
                            new StatusCode("301", "Moved"),
                            new StatusCode("301", "MovedPermanently"),
                            new StatusCode("300", "MultipleChoices"),
                            new StatusCode("204", "NoContent"),
                            new StatusCode("203", "NonAuthoritativeInformation"),
                            new StatusCode("406", "NotAcceptable"),
                            new StatusCode("404", "NotFound"),
                            new StatusCode("501", "NotImplemented"),
                            new StatusCode("304", "NotModified"),
                            new StatusCode("200", "OK"),
                            new StatusCode("206", "PartialContent"),
                            new StatusCode("402", "PaymentRequired"),
                            new StatusCode("412", "PreconditionFailed"),
                            new StatusCode("407", "ProxyAuthenticationRequired"),
                            new StatusCode("302", "Redirect"),
                            new StatusCode("307", "RedirectKeepVerb"),
                            new StatusCode("303", "RedirectMethod"),
                            new StatusCode("416", "RequestedRangeNotSatisfiable"),
                            new StatusCode("413", "RequestEntityTooLarge"),
                            new StatusCode("408", "RequestTimeout"),
                            new StatusCode("414", "RequestUriTooLong"),
                            new StatusCode("205", "ResetContent"),
                            new StatusCode("303", "SeeOther"),
                            new StatusCode("503", "ServiceUnavailable"),
                            new StatusCode("101", "SwitchingProtocols"),
                            new StatusCode("307", "TemporaryRedirect"),
                            new StatusCode("401", "Unauthorized"),
                            new StatusCode("415", "UnsupportedMediaType"),
                            new StatusCode("306", "Unused"),
                            new StatusCode("426", "UpgradeRequired"),
                            new StatusCode("305", "UseProxy"),
            });

            return collection.Get();
        }
    }
}
