﻿namespace TestMoqApi.StatusCode
{
    public class StatusCode
    {
        public string Code { get; set; }
        public string Description { get; set; }

        public StatusCode() { }

        public StatusCode(string code, string description)
        {
            Code = code;
            Description = description;
        }
    }
}
