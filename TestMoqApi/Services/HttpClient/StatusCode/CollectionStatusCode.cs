﻿using System.Collections.Generic;

namespace TestMoqApi.StatusCode
{
    public class CollectionStatusCode
    {
        public List<StatusCode> Collection { get; set; }

        public CollectionStatusCode()
        {
            Collection = new List<StatusCode>();
        }

        public void Add(StatusCode item)
        {
            Collection.Add(item);
        }

        public void AddList(List<StatusCode> item)
        {
            Collection.AddRange(item);
        }

        public List<StatusCode> Get()
        {
            return this.Collection;
        }
    }
}
