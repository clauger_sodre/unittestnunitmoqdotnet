﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TestMoqApi.StatusCode;

namespace TestMoqApi.RequestSystem
{
    public static class Requestsystem
    {
        public static string StatusCode { get; set; }
        public static async Task<string> PostAsync(string Url, Message Message, int Timeout)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                if (Timeout != 0)
                {
                    HttpClient.Timeout = TimeSpan.FromMilliseconds(Timeout);
                }

                try
                {
                    StringContent StringContent = null;

                    foreach (Header item in Message.Header)
                    {
                        // Add Header
                        if (item.Key == "Authorization")
                        {
                            HttpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }

                        if (item.Key == "Content-Type")
                        {
                            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(item.Value));

                            // Add Body
                            switch (item.Value)
                            {
                                case "application/json":
                                    Dictionary<string, object> DictionaryBody = new Dictionary<string, object>();

                                    foreach (Body itemBody in Message.Body)
                                    {
                                        DictionaryBody.Add(itemBody.Key, itemBody.Value);
                                    }

                                    var optionjson = new JsonSerializerOptions();
                                    optionjson.PropertyNameCaseInsensitive = true;
                                    string JsonSerialize = JsonSerializer.Serialize(DictionaryBody, optionjson);
                                    StringContent = new StringContent(JsonSerialize, Encoding.UTF8, "application/json");
                                    break;
                            }
                        }
                    }

                    using (HttpResponseMessage HttpResponseMessage = await HttpClient.PostAsync(Url, StringContent).ConfigureAwait(continueOnCapturedContext: false))
                    {
                        StatusCode = ReturnStatusCode.GetStatusCode().Where(w => w.Description == HttpResponseMessage.StatusCode.ToString()).Select(s => s.Code).FirstOrDefault();

                        using (HttpContent HttpContent = HttpResponseMessage.Content)
                        {
                            string Result = await HttpContent.ReadAsStringAsync();

                            return Result;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    return e.Message;
                }
                catch (TaskCanceledException)
                {
                    return "";
                }
            }
        }

        public static async Task<string> PostAsyncObject(string Url, MessageObject Message, int Timeout)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                if (Timeout != 0)
                {
                    HttpClient.Timeout = TimeSpan.FromMilliseconds(Timeout);
                }

                try
                {
                    StringContent StringContent = null;

                    foreach (Header item in Message.Header)
                    {
                        // Add Header
                        if (item.Key == "Authorization")
                        {
                            HttpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }

                        if (item.Key == "Content-Type")
                        {
                            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(item.Value));

                            // Add Body
                            switch (item.Value)
                            {
                                case "application/json":
                                    var optionjson = new JsonSerializerOptions();
                                    optionjson.PropertyNameCaseInsensitive = true;
                                    string JsonSerialize = JsonSerializer.Serialize(Message.Body, optionjson);
                                    StringContent = new StringContent(JsonSerialize, Encoding.UTF8, "application/json");
                                    break;
                            }
                        }
                    }

                    using (HttpResponseMessage HttpResponseMessage = await HttpClient.PostAsync(Url, StringContent).ConfigureAwait(continueOnCapturedContext: false))
                    {
                        StatusCode = ReturnStatusCode.GetStatusCode().Where(w => w.Description == HttpResponseMessage.StatusCode.ToString()).Select(s => s.Code).FirstOrDefault();

                        using (HttpContent HttpContent = HttpResponseMessage.Content)
                        {
                            string Result = await HttpContent.ReadAsStringAsync();

                            return Result;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    return e.Message;
                }
                catch (TaskCanceledException)
                {
                    return "";
                }
            }
        }

        public static async Task<string> GetAsyncObject(string Url, MessageObject Message, int Timeout)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                if (Timeout != 0)
                {
                    HttpClient.Timeout = TimeSpan.FromMilliseconds(Timeout);
                }

                try
                {
                    foreach (Header item in Message.Header)
                    {
                        // Add Header
                        if (item.Key == "Authorization")
                        {
                            HttpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }

                        if (item.Key == "Content-Type")
                        {
                            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(item.Value));
                        }
                    }

                    using (HttpResponseMessage HttpResponseMessage = await HttpClient.GetAsync(Url).ConfigureAwait(continueOnCapturedContext: false))
                    {
                        StatusCode = ReturnStatusCode.GetStatusCode().Where(w => w.Description == HttpResponseMessage.StatusCode.ToString()).Select(s => s.Code).FirstOrDefault();
                        using (HttpContent HttpContent = HttpResponseMessage.Content)
                        {
                            string Result = await HttpContent.ReadAsStringAsync();
                            if (!string.IsNullOrEmpty(Result))
                            {
                                return Result;
                            }
                            else
                            {
                                return "sem dados";
                            }
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    return e.Message;
                }
                catch (TaskCanceledException)
                {
                    return "";
                }
            }
        }

        public static async Task<string> PutAsync(string Url, Message Message, int Timeout)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                if (Timeout != 0)
                {
                    HttpClient.Timeout = TimeSpan.FromMilliseconds(Timeout);
                }

                try
                {
                    StringContent StringContent = null;

                    foreach (Header item in Message.Header)
                    {
                        // Add Header
                        if (item.Key == "Authorization")
                        {
                            HttpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }

                        if (item.Key == "Content-Type")
                        {
                            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(item.Value));

                            // Add Body
                            switch (item.Value)
                            {
                                case "application/json":
                                    Dictionary<string, object> DictionaryBody = new Dictionary<string, object>();

                                    foreach (Body itemBody in Message.Body)
                                    {
                                        DictionaryBody.Add(itemBody.Key, itemBody.Value);
                                    }

                                    var optionjson = new JsonSerializerOptions();
                                    optionjson.PropertyNameCaseInsensitive = true;
                                    string JsonSerialize = JsonSerializer.Serialize(DictionaryBody, optionjson);
                                    StringContent = new StringContent(JsonSerialize, Encoding.UTF8, "application/json");
                                    break;
                            }
                        }
                    }

                    using (HttpResponseMessage HttpResponseMessage = await HttpClient.PutAsync(Url, StringContent).ConfigureAwait(continueOnCapturedContext: false))
                    {
                        StatusCode = ReturnStatusCode.GetStatusCode().Where(w => w.Description == HttpResponseMessage.StatusCode.ToString()).Select(s => s.Code).FirstOrDefault();

                        using (HttpContent HttpContent = HttpResponseMessage.Content)
                        {
                            string Result = await HttpContent.ReadAsStringAsync();

                            return Result;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    return e.Message;
                }
                catch (TaskCanceledException)
                {
                    return "";
                }
            }
        }

        public static async Task<string> PutAsyncObject(string Url, MessageObject Message, int Timeout)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                if (Timeout != 0)
                {
                    HttpClient.Timeout = TimeSpan.FromMilliseconds(Timeout);
                }

                try
                {
                    StringContent StringContent = null;

                    foreach (Header item in Message.Header)
                    {
                        // Add Header
                        if (item.Key == "Authorization")
                        {
                            HttpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }

                        if (item.Key == "Content-Type")
                        {
                            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(item.Value));

                            // Add Body
                            switch (item.Value)
                            {
                                case "application/json":
                                    var optionjson = new JsonSerializerOptions();
                                    optionjson.PropertyNameCaseInsensitive = true;
                                    string JsonSerialize = JsonSerializer.Serialize(Message.Body, optionjson);
                                    StringContent = new StringContent(JsonSerialize, Encoding.UTF8, "application/json");
                                    break;
                            }
                        }
                    }

                    using (HttpResponseMessage HttpResponseMessage = await HttpClient.PutAsync(Url, StringContent).ConfigureAwait(continueOnCapturedContext: false))
                    {
                        StatusCode = ReturnStatusCode.GetStatusCode().Where(w => w.Description == HttpResponseMessage.StatusCode.ToString()).Select(s => s.Code).FirstOrDefault();
                        using (HttpContent HttpContent = HttpResponseMessage.Content)
                        {
                            string Result = await HttpContent.ReadAsStringAsync();

                            return Result;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    return e.Message;
                }
                catch (TaskCanceledException)
                {
                    return "";
                }
            }
        }

        public static async Task<string> DeleteAsync(string Url, Message Message, int Timeout)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                if (Timeout != 0)
                {
                    HttpClient.Timeout = TimeSpan.FromMilliseconds(Timeout);
                }

                try
                {
                    StringContent StringContent = null;

                    foreach (Header item in Message.Header)
                    {
                        // Add Header
                        if (item.Key == "Authorization")
                        {
                            HttpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }

                        if (item.Key == "Content-Type")
                        {
                            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(item.Value));

                            // Add Body
                            switch (item.Value)
                            {
                                case "application/json":
                                    Dictionary<string, object> DictionaryBody = new Dictionary<string, object>();

                                    foreach (Body itemBody in Message.Body)
                                    {
                                        DictionaryBody.Add(itemBody.Key, itemBody.Value);
                                    }

                                    var optionjson = new JsonSerializerOptions();
                                    optionjson.PropertyNameCaseInsensitive = true;
                                    string JsonSerialize = JsonSerializer.Serialize(DictionaryBody, optionjson);
                                    StringContent = new StringContent(JsonSerialize, Encoding.UTF8, "application/json");
                                    break;
                            }
                        }
                    }

                    var request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Delete,
                        RequestUri = new Uri(Url),
                        Content = StringContent
                    };

                    using (HttpResponseMessage HttpResponseMessage = await HttpClient.SendAsync(request).ConfigureAwait(continueOnCapturedContext: false))
                    {
                        StatusCode = ReturnStatusCode.GetStatusCode().Where(w => w.Description == HttpResponseMessage.StatusCode.ToString()).Select(s => s.Code).FirstOrDefault();

                        using (HttpContent HttpContent = HttpResponseMessage.Content)
                        {
                            string Result = await HttpContent.ReadAsStringAsync();

                            return Result;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    return e.Message;
                }
                catch (TaskCanceledException)
                {
                    return "";
                }
            }
        }

        public static async Task<string> DeleteAsyncObject(string Url, MessageObject Message, int Timeout)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                if (Timeout != 0)
                {
                    HttpClient.Timeout = TimeSpan.FromMilliseconds(Timeout);
                }

                try
                {
                    StringContent StringContent = null;

                    foreach (Header item in Message.Header)
                    {
                        // Add Header
                        if (item.Key == "Authorization")
                        {
                            HttpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }

                        if (item.Key == "Content-Type")
                        {
                            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(item.Value));

                            // Add Body
                            switch (item.Value)
                            {
                                case "application/json":
                                    var optionjson = new JsonSerializerOptions();
                                    optionjson.PropertyNameCaseInsensitive = true;
                                    string JsonSerialize = JsonSerializer.Serialize(Message.Body, optionjson);
                                    StringContent = new StringContent(JsonSerialize, Encoding.UTF8, "application/json");
                                    break;
                            }
                        }
                    }

                    var request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Delete,
                        RequestUri = new Uri(Url),
                        Content = StringContent
                    };

                    using (HttpResponseMessage HttpResponseMessage = await HttpClient.SendAsync(request).ConfigureAwait(continueOnCapturedContext: false))
                    {
                        StatusCode = ReturnStatusCode.GetStatusCode().Where(w => w.Description == HttpResponseMessage.StatusCode.ToString()).Select(s => s.Code).FirstOrDefault();

                        using (HttpContent HttpContent = HttpResponseMessage.Content)
                        {
                            string Result = await HttpContent.ReadAsStringAsync();

                            return Result;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    return e.Message;
                }
                catch (TaskCanceledException)
                {
                    return "";
                }
            }
        }
    }
}
