using TestMoqApi.Model.Token;
using TestMoqApi.Models.Dto;
using TestMoqApi.Services.Token;

namespace TestMoqApi.Services
{
    public class GetServices<T> : IGetServices<T>
    {
        private readonly IToken _Token;
        public GetServices(IToken Token)
        {
            _Token = Token;
        }
        public object GetFlowers(GetFlowersDto parameters)
        {
            GenericToken TokenReceived = _Token.GetToken();
            object range = new { name = "Flowers", start = 0, size = 10, token = TokenReceived };
            return range;
        }

        public object GetIceCream(GetIceCreamDto parameters)
        {
            GenericToken TokenReceived = _Token.GetToken();
            object range = new { name = "IceCreams", start = 0, size = 10, token = TokenReceived };
            return range;
        }
    }
}