using TestMoqApi.Models.Dto;
namespace TestMoqApi.Services
{
    public interface IGetServices<T>
    {
        object GetFlowers(GetFlowersDto parameters);
        object GetIceCream(GetIceCreamDto parameters);
    }
}