using System;
using Microsoft.Extensions.Configuration;
using TestMoqApi.Model.Token;
using TestMoqApi.Models.Credentials;

namespace TestMoqApi.Services.Token
{
    public class TokenService : IToken
    {
        public GenericToken GetToken()
        {
            Credentials userToken = TestMoqApi.Startup.StaticConfig.GetSection("CredentialSettings").Get<Credentials>();
            GenericToken newToken = new GenericToken("eytoe.foadr.kjfaoidur", DateTime.Now, "eyeor.fffaydf.teefaf");
            return newToken;
        }
    }
}