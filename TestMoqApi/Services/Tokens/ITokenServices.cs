using TestMoqApi.Model.Token;

namespace TestMoqApi.Services.Token
{
    public interface IToken
    {
         GenericToken GetToken();
    }
}