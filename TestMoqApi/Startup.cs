using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using TestMoqApi.Business;
using TestMoqApi.Business.Implementation;
using TestMoqApi.Filter;
using TestMoqApi.Services;
using TestMoqApi.Services.Token;

namespace TestMoqApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            StaticConfig = configuration;
        }

        public IConfiguration Configuration { get; }
        public static IConfiguration StaticConfig { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Adicionado filtro para retornar User http StatusCode
            services
                .AddControllers(options =>
                    options.Filters.Add(new HttpResponseExceptionFilter()));

            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("doc", new OpenApiInfo
                    {
                        Version = "v1",
                        Title = "API Test Moq",
                        Description = "Teste with Moq framework"
                    });

                    // Set the comments path for the Swagger JSON and UI.
                    var xmlFile =
                        $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath =
                        Path.Combine(AppContext.BaseDirectory, xmlFile);
                    c.IncludeXmlComments(xmlPath);
                });
            // Mapeados implementações das interfaces
            services.AddScoped(typeof(IBusinessGet<>), typeof(BusinessGet<>));
            services.AddScoped(typeof(IGetServices<>), typeof(GetServices<>));
            services.AddScoped(typeof(IToken), typeof(TokenService));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/doc/swagger.json", "API Test Moq V1");
                });

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);

            app.UseRouting();

            app.UseAuthorization();

            app
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }
    }
}
