﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestMoqApi.Business;
using TestMoqApi.Models.Dto;

namespace TestMoqApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestMoqController : ControllerBase
    {
        private readonly ILogger<TestMoqController> _logger;

        private readonly IBusinessGet<object> _testMoqBusiness;

        public TestMoqController(
            ILogger<TestMoqController> logger,
            IBusinessGet<object> TestMoqBusiness
        )
        {
            _logger = logger;
            _testMoqBusiness = TestMoqBusiness;
        }

        /** <summary>
         Get some tipe of flowers.
         </summary>
         <remarks>
         ### Rules for request: ###
        
               1. Send your name and some number between 0 to 9.
        x
                     Exemplo:
        
                     {
                        "Name": "MyName",
                        "RandomNumber": "Some number"
                     }
        
         </remarks>
         <param name="request"></param>
         <returns> the response.</returns>
        */
        [HttpPost("Flowers")]
        [ProducesResponseType(200, Type = typeof(string))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public IActionResult PostFlowers([FromBody] GetFlowersDto request)
        {
            return new JsonResult(_testMoqBusiness.GetFlowers(request));
        }

        /** <summary>
         Get some type of IceCream.
         </summary>
         <remarks>
         ### Rules for request: ###
        
               1. Send your name and some number between 0 to 9.
        x
                     Exemplo:
        
                     {
                        "Name": "MyName",
                        "RandomNumber": "Some number"
                     }
        
         </remarks>
         <param name="request"></param>
         <returns>The response.</returns>
         */
        [HttpPost("IceCreams")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public IActionResult PostIceCream([FromBody] GetIceCreamDto request)
        {
            return new JsonResult(_testMoqBusiness.GetIceCream(request));
        }
    }
}
