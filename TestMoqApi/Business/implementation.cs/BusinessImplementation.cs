using Microsoft.AspNetCore.Http;
using TestMoqApi.Filter.CustomExeption;
using TestMoqApi.Model.Token;
using TestMoqApi.Models.Dto;
using TestMoqApi.Services;
using TestMoqApi.Services.Token;

namespace TestMoqApi.Business.Implementation
{
    public class BusinessGet<Object> : IBusinessGet<object>
    {
        private readonly IGetServices<object> _getServices;

        public BusinessGet(IGetServices<object> getServices)
        {
            _getServices = getServices;
        }

        public object GetFlowers(GetFlowersDto parameters)
        {
            if (string.IsNullOrEmpty(parameters.Name))
            {
                throw new CustomCodeException(CustomCodeExceptionMensages.NameCantBeNull, StatusCodes.Status400BadRequest);
            }
            return _getServices.GetFlowers(parameters);
        }

        public object GetIceCream(GetIceCreamDto parameters)
        {
            if (string.IsNullOrEmpty(parameters.Name))
            {
                throw new CustomCodeException(CustomCodeExceptionMensages.NameCantBeNull, StatusCodes.Status400BadRequest);
            }

            return _getServices.GetIceCream(parameters);
        }
    }
}
