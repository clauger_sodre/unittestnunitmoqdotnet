using TestMoqApi.Models.Dto;
namespace TestMoqApi.Business
{
    public interface IBusinessGet<T>
    {
        object GetFlowers(GetFlowersDto parameters);
        object GetIceCream(GetIceCreamDto parameters);
    }
}
