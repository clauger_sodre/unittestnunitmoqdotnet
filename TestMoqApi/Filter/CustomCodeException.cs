namespace TestMoqApi.Filter.CustomExeption
{
    public class CustomCodeException : UException
    {
        public override int StatusCode { get; }

        public CustomCodeException(string error, int statusCode) : base(error)
        {
            StatusCode = statusCode;
        }

        public CustomCodeException(string[] errors, int statusCode) : base(errors)
        {
            StatusCode = statusCode;
        }
    }
}