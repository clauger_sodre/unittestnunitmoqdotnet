using System;

namespace TestMoqApi.Filter
{
    public abstract class UException : Exception
    {
        public string[] Errors { get; }
        public abstract int StatusCode { get; }

        protected UException(string error) : base(error)
        {
            Errors = new[] { error };
        }

        protected UException(string[] errors)
        {
            Errors = errors;
        }
    }
}