namespace TestMoqApi.Models.Credentials
{
    public interface ICredentials
    {
        public string User { get; set; }
        public object Password { get; set; }
    }
    public class Credentials : ICredentials
    {
        public string User { get; set; }
        public object Password { get; set; }
    }
}