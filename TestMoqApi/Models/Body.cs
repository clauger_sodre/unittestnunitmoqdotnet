﻿namespace TestMoqApi.RequestSystem
{
    public class Body
    {
        public string Key { get; set; }
        public object Value { get; set; }
    }
}
