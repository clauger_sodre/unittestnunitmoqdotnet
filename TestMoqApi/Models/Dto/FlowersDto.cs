namespace TestMoqApi.Models.Dto
{
    public class GetFlowersDto
    {
        public GetFlowersDto()
        {
        }

        public GetFlowersDto(string name, int randomNumber)
        {
            Name = name;
            RandomNumber = randomNumber;
        }

        public string Name { get; set; }
        public int RandomNumber { get; set; }
    }
}