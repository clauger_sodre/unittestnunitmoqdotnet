namespace TestMoqApi.Models.Dto
{
    public class GetIceCreamDto
    {
       public string Name { get; set; }
        public int RandomNumber { get; set; }
    }
}