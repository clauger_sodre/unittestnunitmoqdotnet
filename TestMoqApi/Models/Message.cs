﻿using System.Collections.Generic;

namespace TestMoqApi.RequestSystem
{
    public class Message
    {
        public List<Header> Header { get; set; }
        public List<Body> Body { get; set; }

        public Message()
        {
            Header = new List<Header>();
            Body = new List<Body>();
        }
    }
}
