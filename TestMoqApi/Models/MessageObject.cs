﻿using System.Collections.Generic;

namespace TestMoqApi.RequestSystem
{
    public class MessageObject
    {
        public List<Header> Header { get; set; }
        public object Body { get; set; }

        public MessageObject()
        {
            Header = new List<Header>();
            Body = new { };
        }
    }
}