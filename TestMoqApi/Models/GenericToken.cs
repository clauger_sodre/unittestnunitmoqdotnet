using System;

namespace TestMoqApi.Model.Token
{
    public class GenericToken
    {
        public GenericToken()
        {
        }

        public GenericToken(string token, DateTime expiration, string refresToken)
        {
            Token = token;
            Expiration = expiration;
            RefresToken = refresToken;
        }

        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public string RefresToken { get; set; }
    }
}